﻿//using System.Runtime.Remoting.Messaging;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Style", menuName = "Style")]
public class Style : ScriptableObject
{
    [Header("Road")]
    public Color32 RoadColor;
    public Color32 RoadEmission;

    [Header("Player")]
    public Color32 PlayerMainColor;
    public Color32 PlayerEmission;
    public Gradient TrailGradient;

    [Header("Spikes")]
    public Color32 SpikesColor;
    public Color32 SpikesEmission;

    [Header("Checkpoints")]
    public Color32 CheckpointColor;
    [ColorUsage(true, true)]
    public Color32 CheckpointEmission;

    [Header("Backgroud")]
    public Color32 TopColor;
    public Color32 MidleColor;
    public Color32 BottomColor;
    public Color32 FogColor;

    [Header("Progress bar")]
    public Color32 MainColor;
    public Color32 BackCollor;
}
