﻿using UnityEngine;
using UnityEngine.UI;

public class StyleManager : MonoBehaviour
{
    [SerializeField]
    private Style[] _styles;
    [SerializeField]
    private Material _road;
    [SerializeField]
    private Material _player;
    [SerializeField]
    private TrailRenderer _playerTrail;
    [SerializeField]
    private Material _spikes;
    [SerializeField]
    private Material _checkpoints;
    [SerializeField]
    private Material _backgroud;
    [SerializeField]
    private Image _progressBarMain;
    [SerializeField]
    private Image _progressBarBack;

    public void SetStyle(int level)
    {
        Random.InitState(level);
        Style style = _styles[Random.Range(0, _styles.Length)];

        _road.SetColor("_Color", style.RoadColor);
        _road.SetColor("_Emission", style.RoadEmission);
        _player.SetColor("_Color", style.PlayerMainColor);
        _player.SetColor("_Emission", style.PlayerEmission);
        _playerTrail.colorGradient = style.TrailGradient;
        _spikes.SetColor("_Color", style.SpikesColor);
        _spikes.SetColor("_Emission", style.SpikesEmission);
        _checkpoints.SetColor("_Color", style.CheckpointColor);
        _checkpoints.SetColor("_EmissionColor", style.CheckpointEmission);
        _backgroud.SetColor("_SkyColorTop", style.TopColor);
        _backgroud.SetColor("_SkyColorHorizon", style.MidleColor);
        _backgroud.SetColor("_SkyColorBottom", style.BottomColor);
        RenderSettings.fogColor = style.FogColor;
        _progressBarMain.color = style.MainColor;
        _progressBarBack.color = style.BackCollor;
    }
}
