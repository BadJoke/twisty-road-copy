﻿using Assets;
using UnityEngine;

public class MoveController : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private Transform _ball;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _rotateSpeed;
    [SerializeField]
    private float _turnSpeed = 0.5f;
    [SerializeField]
    private float _deathZone = 3;

    private float _startPosition;
    private float _endPosition;
    private bool _isRotate = false;
    private Vector3 _turn;

    private void Start()
    {
        Options.MovementSpeed = _speed;
        Options.RotationSpeed = _rotateSpeed;
    }

    private void Update()
    {
        _rigidbody.MovePosition(_rigidbody.position + transform.forward * Options.ActiveMovementSpeed * Time.deltaTime);
        _ball.Rotate(Vector3.right * Options.ActiveRotationSpeed * Time.deltaTime);

        if (Input.GetMouseButtonDown(0))
        {
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        }

        if (Input.GetMouseButton(0))
        {
            Rotation();
        }

        if (Input.GetMouseButtonUp(0))
        {
            _isRotate = false;
            _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        }
    }

    private void Rotation()
    {
        if (_isRotate == false)
        {
            _isRotate = true;
            _startPosition = Input.mousePosition.x;
        }

        _endPosition = Input.mousePosition.x;

        if (Mathf.Abs(_startPosition - _endPosition) >= _deathZone && Options.IsPlaying)
        {
            if (_startPosition < _endPosition)
            {
                _turn = Vector3.up * _turnSpeed * Time.deltaTime;
            }
            else if (_startPosition > _endPosition)
            {
                _turn = Vector3.up * -_turnSpeed * Time.deltaTime;
            }
            else
            {
                _turn = Vector3.zero;
            }
        }
        else
        {
            _turn = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
        }

        transform.Rotate(_turn);
    }
}
