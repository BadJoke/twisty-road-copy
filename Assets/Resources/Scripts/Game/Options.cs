﻿using UnityEngine;

namespace Assets
{
    public static class Options
    {
        public static float MovementSpeed;
        public static float ActiveMovementSpeed;
        public static float RotationSpeed;
        public static float ActiveRotationSpeed;
        public static float GemRotationSpeed;
        public static int Level;
        public static bool IsPlaying;
        public static bool IsWining = true;
        public static Transform GemTarget;
    }
}