﻿using Assets;
using PathCreation;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LevelCustomizer : MonoBehaviour
{
    [Header("Settings")]
    
    [SerializeField]
    private Text _levelText;
    [SerializeField]
    private Text _menuLevelText;
    [SerializeField]
    private GameObject _menuPanel;
    [SerializeField]
    private PathCreator[] _trails;
    [SerializeField]
    private GameObject _player;
    [SerializeField]
    private MeshRenderer _ball;

    [Header("Gems Settings")]
    [SerializeField]
    private float _gemRrotateSpeed;
    [SerializeField]
    private Transform _gemTarget;

    [Header("Another scripts")]
    [SerializeField]
    private Collector _collector;
    [SerializeField]
    private ParticlePooler _particlePooler;
    [SerializeField]
    private StyleManager _styleManager;
    [SerializeField]
    private TrailRenderer _trail;

    [Header("Spawn options")]
    [SerializeField]
    private int _oneObjectCount;
    [SerializeField]
    private GameObject[] _prefabs;
    [SerializeField]
    private int _maxStrike;

    [Header("Offset and Level options")]
    [SerializeField]
    private int _level;
    [SerializeField]
    private int _checkedLevelCount;
    [SerializeField]
    private float[] _offsetByLevels;
    [SerializeField]
    private float _curOffset;

    private float _playerSpawn;
    private List<GameObject>[] _pathObjects;
    private int _generalCount;
    private float _generalLength;
    private float _step;

    private void Awake()
    {
        LoadLevel();
    }

    public void LoadLevel()
    {
        if (_level == 0 || _level < Options.Level)
        {
            if (PlayerPrefs.HasKey("Level"))
            {
                Options.Level = PlayerPrefs.GetInt("Level");
            }
            else
            {
                Options.Level = 1;
            }

            _level = Options.Level;
        }
        else
        {
            Options.Level = _level;
        }

        // Offset options
        if (_level > _checkedLevelCount)
            _curOffset = 0;
        else
            _curOffset = _offsetByLevels[_level-1];

        _levelText.text = "LEVEL " + _level;
        _menuLevelText.text = _levelText.text;
        _menuPanel.SetActive(true);
        _collector.ClearProgress();
        _styleManager.SetStyle(_level);
        _ball.enabled = true;
        _ball.transform.rotation = new Quaternion(0, 0, 0, 0);

        Options.ActiveMovementSpeed = 0;
        Options.ActiveRotationSpeed = 0;
        Options.GemRotationSpeed = _gemRrotateSpeed;
        Options.GemTarget = _gemTarget;
        Options.IsPlaying = false;

        Random.InitState(_level);

        _generalCount = _oneObjectCount * _prefabs.Length + 1;
        _generalLength = _trails[0].path.length + _trails[1].path.length;
        _step = _generalLength / _generalCount;

        SpawnPlayer();

        if (Options.IsWining)
        {
            if (_pathObjects != null)
            {
                for (int i = 0; i < _pathObjects.Length; i++)
                {
                    for (int j = 0; j < _pathObjects[i].Count; j++)
                    {
                        Destroy(_pathObjects[i][j]);
                    }
                }
            }

            _pathObjects = new List<GameObject>[3];

            for (int i = 0; i < _pathObjects.Length; i++)
            {
                _pathObjects[i] = new List<GameObject>();
            }

            FillPath();

            Options.IsWining = false;
        }
        else
        {
            for (int i = 0; i < _pathObjects.Length; i++)
            {
                Respawn(_pathObjects[i], _prefabs[i]);
            }
        }

        _particlePooler.FindGems();
    }

    private void SpawnPlayer()
    {
        int n = Random.Range(0, _generalCount);
        _playerSpawn = n * _step;
        float length = _playerSpawn + _curOffset + Random.Range(0f, 10f);
        int path = 0;

        if (length > _trails[0].path.length)
        {
            length -= _trails[0].path.length;
            path = 1;
        }

        _player.transform.GetChild(0).gameObject.SetActive(true);
        _player.transform.position = _trails[path].path.GetPointAtDistance(length) + Vector3.up / 2;

        Quaternion rotation = _trails[path].path.GetRotationAtDistance(length);
        rotation.x = 0;
        rotation.z = 0;
        _player.transform.rotation = rotation;
        _trail.enabled = true;

        if (Random.Range(0, 2) == 1)
        {
            _player.transform.Rotate(Vector3.up * 180);
        }
    }

    private void Respawn(List<GameObject> list, GameObject prefab)
    {
        for (int i = 0; i < list.Count; i++)
        {
            GameObject gameObject = Instantiate(
                prefab,
                list[i].transform.position,
                list[i].transform.rotation,
                list[i].transform.parent);

            Destroy(list[i]);
            list[i] = gameObject;
        }
    }

    private void FillPath()
    {
        int[] counters = new int[_pathObjects.Length];
        int[] ratings = new int[_pathObjects.Length];
        int prefab;

        for (int i = 0; i < _generalCount; i++)
        {
            float length = i * _step;
            int path = 0;

            if (length == _playerSpawn)
            {
                continue;
            }

            length += Random.Range(0f, 10f) + _curOffset;

            if (length > _generalLength)
            {
                length -= _generalLength;
                path = 0;
            }

            if (length > _trails[0].path.length)
            {
                length -= _trails[0].path.length;
                path = 1;
            }

            do
            {
                int minValue = Mathf.Min(ratings);

                if (minValue <= -_maxStrike)
                {
                    prefab = Array.IndexOf(ratings, minValue);
                }
                else
                {
                    prefab = Random.Range(0, _prefabs.Length);
                }

                if (counters[prefab] >= _maxStrike)
                {
                    continue;
                }

                if (_pathObjects[prefab].Count < _oneObjectCount)
                {
                    for (int j = 0; j < counters.Length; j++)
                    {
                        if (j != prefab)
                        {
                            counters[j] = 0;
                            ratings[j]--;
                        }
                    }
                    ratings[prefab] += 2;
                    break;
                }
            } while (true);

            counters[prefab]++;

            Spawn(path, length, _prefabs[prefab], _pathObjects[prefab]);
        }
    }

    private void Spawn(int path, float length, GameObject prefab, List<GameObject> list)
    {
        Vector3 position = _trails[path].path.GetPointAtDistance(length);

        Quaternion rotation = _trails[path].path.GetRotationAtDistance(length);
        GameObject go = Instantiate(prefab, position, rotation, _trails[path].transform);
        go.transform.Rotate(Vector3.forward * 90);
        list.Add(go);
    }
}
