﻿using System.Collections;
using UnityEngine;

public class CheckPointHandler : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _particles;

    private void OnTriggerEnter(Collider other)
    {
        StartCoroutine(Blust());
    }

    private IEnumerator Blust()
    {
        var main = _particles.main;
        main.loop = false;
        main.startLifetime = 0.5f;
        main.startSpeed = 10;

        yield return new WaitForSeconds(1);

        _particles.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
