﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticlePooler : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _particle;
    private Queue<ParticleSystem> _particles;

    private void Start()
    {
        _particles = new Queue<ParticleSystem>();

        for (int i = 0; i < 5; i++)
        {
            _particles.Enqueue(Instantiate(_particle, transform));
        }
    }

    public void FindGems()
    {
        Gem[] gems = FindObjectsOfType<Gem>();
        foreach (var gem in gems)
        {
            gem.Triggerer.AddListener(Play);
        }
    }

    private void Play(Vector3 position)
    {
        StartCoroutine("PlayEffect", position);
    }

    private IEnumerator PlayEffect(object pos)
    {
        Vector3 position = (Vector3)pos;
        ParticleSystem particle = _particles.Dequeue();
        particle.transform.position = position;
        particle.gameObject.SetActive(true);
        particle.Play();
        yield return new WaitForSeconds(0.5f);
        particle.gameObject.SetActive(false);
        _particles.Enqueue(particle);
    }
}
