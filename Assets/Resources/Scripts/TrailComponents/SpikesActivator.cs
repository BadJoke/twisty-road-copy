﻿using System.Collections;
using UnityEngine;
using Random = System.Random;

public class SpikesActivator : MonoBehaviour
{
    [SerializeField]
    private float _replacingTime = 1.5f;
    [SerializeField]
    private float _risingSpeed = 0.1f;

    [SerializeField]
    private GameObject[] _spikes;
    [SerializeField]
    private BoxCollider[] _spikeColliders;

    private Random _random;

    private int[] _actives;
    private int[] _disactives;
    private float _waitingTime = 0.01f;
    private int _times = 10;
    private bool _canTrigger;

    private void Start()
    {
        _random = new Random();
        _actives = new int[] { _random.Next(0, _spikes.Length), 0 };
        _disactives = new int[] { _random.Next(0, _spikes.Length), 0 };

        int i;
        do
        {
            i = _random.Next(0, _spikes.Length);
        } while (i == _actives[0]);

        _actives[1] = i;

        for (int j = 0; j < _actives.Length; j++)
        {
            _disactives[j] = _actives[j];
        }

        FindObjectOfType<MenuOffer>().Touch.AddListener(StartWorking);
    }

    private void OnTriggerEnter(Collider other)
    {
        CancelInvoke();

        if (_canTrigger)
        {
            if (_random.Next(0, 100) < 50)
            {
                Invoke("Working", 0.1f);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        InvokeRepeating("Working", 0f, _replacingTime);
    }

    private void SwitchActives()
    {
        _disactives[0] = _actives[0];
        _disactives[1] = _actives[1];
        int newI1;
        int newI2;

        _spikeColliders[_disactives[0]].enabled = false;
        _spikeColliders[_disactives[1]].enabled = false;

        do
        {
            newI1 = _random.Next(0, _spikes.Length);
        } while (newI1 == _disactives[0] || newI1 == _disactives[1]);

        do
        {
            newI2 = _random.Next(0, _spikes.Length);
        } while (newI2 == _disactives[0] || newI2 == _disactives[1] ||  newI2 == newI1);

        _actives[0] = newI1;
        _actives[1] = newI2;
        _spikeColliders[newI1].enabled = true;
        _spikeColliders[newI2].enabled = true;
    }

    private IEnumerator Toggle(int[] spikes, float direction)
    {
        if (direction == 0)
        {
            _canTrigger = false;
        }

        Transform spike1 = _spikes[spikes[0]].transform;
        Transform spike2 = _spikes[spikes[1]].transform;
        Vector3 scale1 = new Vector3(spike1.localScale.x, direction, spike1.localScale.z);
        Vector3 scale2 = new Vector3(spike2.localScale.x, direction, spike2.localScale.z);

        for (int i = 0; i < _times; i++)
        {
            spike1.localScale = Vector3.MoveTowards(spike1.localScale, scale1, _risingSpeed);
            spike2.localScale = Vector3.MoveTowards(spike2.localScale, scale2, _risingSpeed);
            yield return new WaitForSeconds(_waitingTime);
        }

        if (direction == 1)
        {
            yield return new WaitForSeconds(0.9f);
            _canTrigger = true;
        }
    }

    private void Working()
    {
        SwitchActives();
        StartCoroutine(Toggle(_disactives, 0f));
        StartCoroutine(Toggle(_actives, 1f));
    }

    private void StartWorking()
    {
        InvokeRepeating("Working", 0, _replacingTime);
    }
}
