﻿using Assets;
using UnityEngine;

public class TrapHandler : MonoBehaviour
{
    [SerializeField]
    private MeshRenderer _ball;
    [SerializeField]
    private TrailRenderer _trail;
    [SerializeField]
    private ParticleSystem _dieEffect;
    [SerializeField]
    private PanelsController _panelsController; 

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer.Equals(11) && Options.IsPlaying)
        {
            _ball.enabled = false;
            _trail.enabled = false;
            _dieEffect.Play();
            _panelsController.Lose();
        }
    }
}
