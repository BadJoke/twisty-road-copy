﻿using Assets;
using UnityEngine;
using UnityEngine.Events;

public class Gem : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _rigidbody;

    public Trigger Triggerer;

    private float _angle;
    private float _maxAngle;
    private float _startY;

    private void Awake()
    {
        _angle = Random.Range(0, Mathf.PI * 2);
        _maxAngle = Mathf.PI * 2;
        _startY = transform.localPosition.y;
        Triggerer = new Trigger();
    }

    private void Update()
    {
        transform.Rotate(Vector3.up * Options.GemRotationSpeed * Time.deltaTime);

        transform.localPosition = new Vector3(
                transform.localPosition.x,
                _startY + Mathf.Sin(_angle) / 6,
                transform.localPosition.z);

        _angle += Time.deltaTime;
        _angle = _angle < _maxAngle ? _angle : 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Player"))
        {
            Triggerer.Invoke(transform.position);
            gameObject.SetActive(false);
        }
    }

    public class Trigger : UnityEvent<Vector3> { }
}
