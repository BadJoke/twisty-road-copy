﻿using Assets;
using UnityEngine;
using UnityEngine.UI;

public class BlackPanelController : MonoBehaviour
{
    [SerializeField]
    private LevelCustomizer _customizer;
    [SerializeField]
    private Button _menu;
    [SerializeField]
    private Animator _interfaceAnimator;
    [SerializeField]
    private Animator _blackPanelAnimator;

    public void LoadLevel()
    {
        _customizer.LoadLevel();
        _menu.interactable = true;
    }

    public void Disable()
    {
        _blackPanelAnimator.ResetTrigger("End");
        gameObject.SetActive(false);

        if (Options.IsWining)
        {
            _interfaceAnimator.ResetTrigger("Win");
        }
        else
        {
            _interfaceAnimator.ResetTrigger("Lose");
        }
    }
}
