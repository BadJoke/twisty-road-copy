﻿using Assets;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Collector : MonoBehaviour
{
    [SerializeField]
    private int _winCount;
    [SerializeField]
    private Image _progress;
    [SerializeField]
    private float _fillingRatio;
    [SerializeField]
    private PanelsController _panelsController;
    [SerializeField]
    private Text _gemCountText;

    private int _needToWin;
    private int _checkPointCount;
    private int _gemCount;

    private void Start()
    {
        Random.InitState(Options.Level);
        ClearProgress();

        if (PlayerPrefs.HasKey("GemCount"))
        {
            _gemCount = PlayerPrefs.GetInt("GemCount");
        }

        _gemCountText.text = _gemCount.ToString();
    }

    public void ClearProgress()
    {
        _needToWin = Random.Range(_winCount - 1, _winCount + 2);
        _checkPointCount = 0;
        _progress.fillAmount = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag.Equals("Checkpoint"))
        {
            _checkPointCount++;
            StartCoroutine(ProgressFilling(_progress.fillAmount, (float)_checkPointCount / _needToWin));
        }

        if (other.tag.Equals("Gem"))
        {
            _gemCount++;
            PlayerPrefs.SetInt("GemCount", _gemCount);
            PlayerPrefs.Save();

            _gemCountText.text = _gemCount.ToString();

            Destroy(other.gameObject);
        }
    }

    private IEnumerator ProgressFilling(float startValue, float finishValue)
    {
        float step = (finishValue - startValue) / _fillingRatio;

        while (startValue < finishValue)
        {
            startValue += step;
            _progress.fillAmount = startValue;

            yield return null;
        }

        if (_progress.fillAmount == 1)
        {
            StopAllCoroutines();

            if (Options.IsPlaying)
            {
                Options.IsWining = true;
                _panelsController.Win();
            }
        }
    }
}
