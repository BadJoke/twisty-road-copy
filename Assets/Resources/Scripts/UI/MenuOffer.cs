﻿using Assets;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MenuOffer : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _ball;
    [SerializeField]
    private Animator _menuAnimator;

    public UnityEvent Touch;

    public void OnClick()
    {
        Options.IsPlaying = true;
        Options.ActiveMovementSpeed = Options.MovementSpeed;
        Options.ActiveRotationSpeed = Options.RotationSpeed;
        Button button = GetComponent<Button>();

        button.interactable = false;
        Touch.Invoke();

        _ball.useGravity = true;
        _menuAnimator.SetTrigger("Start");
    }
}
