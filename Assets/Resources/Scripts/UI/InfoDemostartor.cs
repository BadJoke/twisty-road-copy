﻿using UnityEngine;

public class InfoDemostartor : MonoBehaviour
{
    [SerializeField]
    private Animator _infoAnimator;
    [SerializeField]
    private GameObject _menu;

    public void ShowInfo()
    {
        _infoAnimator.SetTrigger("Start");
        _menu.SetActive(false);
    }
}
