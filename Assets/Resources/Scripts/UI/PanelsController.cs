﻿using Assets;
using UnityEngine;
using UnityEngine.UI;

public class PanelsController : MonoBehaviour
{
    [SerializeField]
    private Button _menu;
    [SerializeField]
    private GameObject _blackPanel;
    [SerializeField]
    private GameObject _pausePanel;
    [SerializeField]
    private Text _winMessage;
    [SerializeField]
    private Animator _interfaceAnimator;
    [SerializeField]
    private Animator _blackPanelAnimator;
    [SerializeField]
    private LevelCustomizer _levelCustomizer;
    [SerializeField]
    private Rigidbody _player;

    public void OnClick()
    {
        StopGame(Options.IsWining ? "Win" : "Lose");
    }

    public void OnPauseClick()
    {
        Pause(true, 0 ,0);
    }

    public void OnPauseOffClick()
    {
        Pause(false, Options.MovementSpeed, Options.RotationSpeed);
    }

    public void Win()
    {
        _winMessage.text = "LEVEL " + Options.Level + "\nCOMPLETED!";

        PlayerPrefs.SetInt("Level", ++Options.Level);
        PlayerPrefs.Save();

        StopGame("Win");
    }

    public void Lose()
    {
        StopGame("Lose");
    }

    public void EnableBlackPanel()
    {
        _blackPanel.SetActive(true);
        _blackPanelAnimator.SetTrigger("End");
    }

    private void Pause(bool active, float moveSpeed, float rotateSpee)
    {
        _pausePanel.SetActive(active);
        _player.useGravity = !active;
        Options.IsPlaying = !active;
        Options.ActiveMovementSpeed = moveSpeed;
        Options.ActiveRotationSpeed = rotateSpee;

        if (active)
        {
            _player.velocity = Vector3.zero;
        }
    }

    private void StopGame(string trigger)
    {
        Options.ActiveMovementSpeed = 0;
        Options.ActiveRotationSpeed = 0;
        Options.IsPlaying = false;

        _interfaceAnimator.SetTrigger(trigger);
    }

    public void DisableMenu()
    {
        _menu.gameObject.SetActive(false);
    }
}
